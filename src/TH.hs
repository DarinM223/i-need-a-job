{-# LANGUAGE TemplateHaskell #-}

module TH where

import Control.Concurrent
import Control.Monad
import Language.Haskell.TH
import System.Directory
import System.Process
import System.IO

say :: String -> String -> String
say github project = "By the way, the author of " ++ project ++ " (" ++
  github ++ ") is looking for a job :)"

message :: String -> String -> String
message github project =
  replicate 5 '\n' ++ say github project ++ replicate 6 '\n'

ask :: String -> String -> Int -> Q Exp
ask github project repeat = do
  runIO $ replicateM repeat $ threadDelay 2000000
                           >> putStrLn (message github project)
  [| () |]
